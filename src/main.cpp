#include <Arduino.h>
#include <avr/sleep.h>

// Utility macro
#define adc_disable() (ADCSRA &= ~(1<<ADEN)) // disable ADC (before power-off)

#define spinnerPin 0    // PB0, pin 5 on the ATtiny855 
#define loopCount 150   // in 8 second increments, so 20 minutes

// delay using sleep mode. paramter value:
// 0:15ms 1:30ms 2:60ms 3:120ms 4:250ms 5:500ms 6:1s 7:2s 8:4s 9:8s 10:forever
void WDDelay(int n) {
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  WDTCR = 1<<WDIE | (n & 0x8)<<2 | 1<<WDE | (n & 0x7);
  sleep_enable();
  sleep_cpu();
}

// at initialize
void setup() {
  // immediately ensure motor off
  pinMode (spinnerPin, OUTPUT);
  digitalWrite (spinnerPin, HIGH);
  // Turn off Timer/Counter0, Timer/Counter1, and USI to save power
  PRR = 1<<PRTIM1 | 1<<PRTIM0 | 1<<PRUSI;
  // all pins to input_pullup
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  adc_disable(); // ADC uses ~320uA
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
}

void loop() {
  digitalWrite (spinnerPin, LOW); // motor on
  WDDelay(5); // 0.5 second spin. Gives roughly 10 rotations,
  digitalWrite (spinnerPin, HIGH); // motor off
  for (int i = loopCount; i > 0; i--) // 1 spin per 20 minutes.  so on average 1 rotation per 2 minutes 
    WDDelay(9);
}

ISR(WDT_vect) {
  // Special sequence to disable watchdog timer
  WDTCR = 1<<WDCE | 1<<WDE;
  WDTCR = 0;
}